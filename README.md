
# Portable Resource Compiler for C++11

## What is it ?

A simple tool to embed small resource files into any executable or plugin, providing a platform-independent API for access.

## How does it work ?

It appends the resource data to the already compiled binary.
At runtime the path of the binary is determined by searching through program headers, by having knowledge of the address of the caller instruction. A compiled resource directory is found at the end-of-file and loaded by the runtime.

This method works on Linux, Windows, Mac OS X. It is probably compatible with Android and iOS too, but this has not been tested.

## How to use it

- Compile rescomp. This requires boost libraries to build.

\# make

- Add the resource loader sources into your project.

\# g++ -std=gnu++11 -o hello hello.cc c++/module_helper.cc c++/resources.cc

Note that Boost is not needed for the runtime to work. A compliant C++11 compiler is sufficient.

- Embed the contents of a directory: res

\# rescomp -b hello res/

## Try it

A demo program is found under examples/ to illustrate the usage of the API.
