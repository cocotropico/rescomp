#include "c++/resources"
#include "c++/module_helper"
#include <array>
#include <vector>
#include <mutex>
#include <iostream>
#include <fstream>
#ifdef WIN32
# include <winsock2.h>
#else
# include <arpa/inet.h>
#endif

resource_istream::resource_istream()
  : std::istream(0) {
}

resource_istream::resource_istream(std::string &&x)
  : _x(x)
  , _b(_x, std::ios::in | std::ios::binary)
  , std::istream(&_b) {
}

resource_istream::resource_istream(resource_istream &&s)
  : _x(std::move(s._x))
  , _b(_x, std::ios::in | std::ios::binary)
  , std::istream(&_b) {
}

std::string resource_handle::_res_file_name;
std::map<std::string, resource_handle::resource_info> resource_handle::_res_dir;

resource_handle::resource_handle()
{
  static bool initialized = false;
  static std::mutex mutex_init;
  std::unique_lock<std::mutex> init_locker(mutex_init);

  // one-time init
  if (! initialized) {
    initialized = true;

    _res_file_name = get_current_module_file_name();
    std::ifstream in(_res_file_name.c_str(), std::ios::in | std::ios::binary);

    std::array<char, 4> magic = {0,0,0,0};
    in.seekg(-4, std::ios::end);
    in.read(magic.data(), 4);

    const std::array<char, 4> expected_magic = {'R','e','s','M'};
    if (magic != expected_magic) {
      std::cerr << "invalid resource magic!\n";
      return;
    }

    uint32_t dir_off_from_end;
    in.seekg(-8, std::ios::cur);
    in.read((char *) &dir_off_from_end, 4);
    dir_off_from_end = ntohl(dir_off_from_end);
    in.seekg(-4 - int32_t(dir_off_from_end), std::ios::cur);

    uint32_t dir_off = in.tellg();

    uint32_t num_entries;
    in.read((char *) &num_entries, 4);
    num_entries = ntohl(num_entries);

    for (uint32_t i = 0; i < num_entries; ++i) {
      uint32_t entry_off;
      uint32_t entry_size;
      uint32_t name_len;
      in.read((char *) &entry_off, 4);
      in.read((char *) &entry_size, 4);
      in.read((char *) &name_len, 4);
      entry_off = htonl(entry_off);
      entry_size = htonl(entry_size);
      name_len = htonl(name_len);

      std::vector<char> name_buf;
      name_buf.resize(name_len + 1);
      in.read(&name_buf[0], name_len);
      name_buf[name_len] = '\0';

      uint32_t abs_off = dir_off + entry_off;
      resource_info &r = _res_dir[name_buf.data()];
      r.file_off = abs_off;
      r.size = entry_size;
    }
  }
}

bool resource_handle::read_contents(const char *filename, std::string &contents)
{
  resource_info *r = 0;
  { std::map<std::string, resource_info>::iterator I = _res_dir.find(filename);
    if (I != _res_dir.end())
      r = &I->second; }
  if (! r)
    return false;

  std::ifstream in(_res_file_name.data(), std::ios::in | std::ios::binary);
  in.seekg(r->file_off);

  unsigned size = r->size;
  std::vector<char> buf;
  buf.resize(size);
  in.read((char *) buf.data(), size);

  bool success = in.gcount() == size;
  if (success)
    contents.assign(buf.data(), buf.size());
  return success;
}

resource_istream resource_handle::get_file(const char *filename)
{
  std::string data;
  return read_contents(filename, data) ?
    resource_istream(std::move(data)) : resource_istream();
}

unsigned resource_handle::file_count()
{
  return _res_dir.size();
}

std::list<std::string> resource_handle::file_list()
{
  std::list<std::string> r;
  for (std::pair<const std::string &, resource_info> p: _res_dir)
    r.push_back(p.first);
  return r;
}
