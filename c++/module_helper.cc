#include "c++/module_helper"
#include <vector>
#include <iostream>
#include <exception>
#if defined(__linux__)
# include <limits.h>
# include <unistd.h>
# include <link.h>
#endif
#if defined(__APPLE__)
# include <mach-o/dyld.h>
#endif
#if defined(WIN32)
# include <windows.h>
# include <psapi.h>
#endif

// should not happen
struct module_exception : std::exception {};

uintptr_t get_current_address()
{
  return (uintptr_t) __builtin_return_address(0);
}

std::string get_module_file_name(uintptr_t addr)
{
#if defined(__APPLE__)
  for (uint32_t i = 0, n = _dyld_image_count(); i < n; i++) {
    const mach_header *header = _dyld_get_image_header(i);
    intptr_t offset = _dyld_get_image_vmaddr_slide(i);
    unsigned header_size = (header->magic == MH_MAGIC_64) ?
      sizeof(mach_header_64) : sizeof(mach_header);
    load_command *cmd = (load_command*) ((char *) header + header_size);
    for (uint32_t iCmd = 0, nCmd = header->ncmds; iCmd < nCmd; ++iCmd) {
      switch (cmd->cmd) {
      case LC_SEGMENT: {
        segment_command *seg = (segment_command *) cmd;
        if (((uintptr_t) addr >= (seg->vmaddr + offset)) &&
            ((uintptr_t) addr < (seg->vmaddr + offset + seg->vmsize))) {
          return _dyld_get_image_name(i);
        }
        break;
      }
      case LC_SEGMENT_64: {
        segment_command_64 *seg = (segment_command_64 *) cmd;
        if (((uintptr_t) addr >= (seg->vmaddr + offset)) &&
            ((uintptr_t) addr < (seg->vmaddr + offset + seg->vmsize))) {
          return _dyld_get_image_name(i);
        }
        break;
      }
      }
      cmd = (load_command *) ((char *) cmd + cmd->cmdsize);
    }
  }
  throw module_exception();
#elif defined(__linux__)
  struct dl_cb_struct {
    char name[PATH_MAX + 1];
    uintptr_t addr;
    bool success;
  } dl_cb_data;
  dl_cb_data.addr = addr;
  dl_cb_data.success = false;
  int ret = dl_iterate_phdr([](dl_phdr_info *info, size_t size, void *data) -> int {
      char *name = reinterpret_cast<dl_cb_struct *>(data)->name;
      uintptr_t addr = reinterpret_cast<dl_cb_struct *>(data)->addr;
      bool &success = reinterpret_cast<dl_cb_struct *>(data)->success;
      for (ElfW(Half) i = 0, n = info->dlpi_phnum; i < n; ++i) {
        const ElfW(Phdr) *phdr = &info->dlpi_phdr[i];
        if (addr >= phdr->p_vaddr && addr < phdr->p_vaddr + phdr->p_memsz) {
          if (info->dlpi_name && info->dlpi_name[0]) {
            int n = snprintf(name, PATH_MAX + 1, "%s", info->dlpi_name);
            success = n <= PATH_MAX;
          } else {
            name[0] = '\0';
            success = true;
          }
          return 1;
        }
      }
    }, &dl_cb_data);
  if (dl_cb_data.success) {
    char *name = dl_cb_data.name;
    if (name[0]) {
      return name;
    } else {
      ssize_t n = readlink("/proc/self/exe", name, PATH_MAX);
      if (n >= 0 && n <= PATH_MAX) {
        name[n] = '\0';
        return name;
      }
    }
  }
  throw module_exception();
#elif defined(WIN32)
  std::vector<HMODULE> modules(32);
  for (;;) {
    DWORD cbActual = modules.size() * sizeof(HMODULE);
    DWORD cbNeeded = 0;
#if defined(WIN64)
    BOOL ret = EnumProcessModulesEx(GetCurrentProcess(), modules.data(), cbActual, &cbNeeded, LIST_MODULES_ALL);
#else
    BOOL ret = EnumProcessModules(GetCurrentProcess(), modules.data(), cbActual, &cbNeeded);
#endif
    if (! ret)
      throw module_exception();
    if (cbNeeded <= cbActual)
      break;
    modules.resize(1 + cbNeeded / sizeof(HMODULE));
  }

  for (HMODULE hMod: modules) {
    PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER) hMod;
    PIMAGE_NT_HEADERS ntHdr = (PIMAGE_NT_HEADERS) ((BYTE *) dosHdr + dosHdr->e_lfanew);
    DWORD sizeOfImage = ntHdr->OptionalHeader.SizeOfImage;
    if (addr >= (uintptr_t) hMod && addr < (uintptr_t) hMod + sizeOfImage) {
      std::vector<char> fileName(MAX_PATH);
      for (;;) {
        DWORD nSize = GetModuleFileNameA(hMod, fileName.data(), fileName.size());
        if (nSize < fileName.size()) {
          return std::string(fileName.begin(), fileName.begin() + nSize);
        }
        if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
          throw module_exception();
      }
    }
  }
#else
# error not implemented on this OS
#endif
}

std::string get_current_module_file_name()
{
  uintptr_t addr = get_current_address();
  return get_module_file_name(addr);
}
