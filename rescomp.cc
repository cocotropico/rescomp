
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/detail/endian.hpp>
#include <list>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <arpa/inet.h>

namespace fs = boost::filesystem;
namespace po = boost::program_options;

struct resource_info {
  std::string name;
  std::string file_path;
  unsigned file_size;
  unsigned off;
};

static std::list<resource_info *> all_res;

static void collect_directory_resources(const fs::path &assets_dir)
{
  for (fs::recursive_directory_iterator iter_assets(assets_dir);
       iter_assets != fs::recursive_directory_iterator(); ++iter_assets) {
    fs::path f_path = *iter_assets;
    if (fs::is_regular_file(f_path)) {
      unsigned level = iter_assets.level();
      fs::path tr_path;
      for (fs::path::iterator I = f_path.begin(), E = f_path.end(); I != E; ++I)
        if (std::distance(I, E) <= level + 1)
          tr_path /= *I;
      resource_info *r = new resource_info;
      r->name = tr_path.generic_string();
      r->file_path = f_path.string();
      r->file_size = fs::file_size(f_path);
      all_res.push_back(r);
    }
  }
}

static void append_resources(const fs::path &bin_file)
{
  // compute resource offsets
  unsigned cur_off = 0;
  for (resource_info *r: all_res) {
    r->off = cur_off;
    cur_off += r->file_size;
  }

  std::ofstream out(bin_file.string(), std::ios::in | std::ios::out | std::ios::binary | std::ios::ate);

  uint32_t dir_off = out.tellp();

  // compute the size of the resource directory
  unsigned dir_size = 4;
  for (resource_info *r: all_res) {
    dir_size += 4; // entry off (relative to directory start)
    dir_size += 4; // entry size
    dir_size += 4; // name length
    dir_size += r->name.size(); // name
  }

  // write the directory
  uint32_t num_entries = htonl(all_res.size());
  out.write((char *) &num_entries, 4);
  for (resource_info *r: all_res) {
    uint32_t entry_off = htonl(dir_size + r->off);
    uint32_t entry_size = htonl(r->file_size);
    uint32_t name_len = htonl(r->name.size());
    out.write((char *) &entry_off, 4);
    out.write((char *) &entry_size, 4);
    out.write((char *) &name_len, 4);
    out << r->name;
  }

  // write the resources
  for (resource_info *r: all_res) {
    std::ifstream in_data(r->file_path.c_str(), std::ios::in | std::ios::binary);
    out << in_data.rdbuf();
  }

  // write the relative directory offset from the beginning of the epilog
  uint32_t dir_off_from_end = htonl(uint32_t(out.tellp()) - dir_off);
  out.write((char *) &dir_off_from_end, 4);
  // write the magic number
  char magic[4] = {'R','e','s','M'};
  out.write(magic, 4);
}

int main(int argc, char **argv)
{
  po::options_description desc("Allowed options");
  desc.add_options()
    ("bin,b", po::value<std::string>(), "path to executable")
    ("input-dir", po::value<std::vector<std::string>>(), "input directories")
    ;

  po::positional_options_description p;
  p.add("input-dir", -1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  if (! vm.count("bin")) {
    std::cerr << "error: no executable was specified.\n";
    std::cerr << desc;
    exit(1);
  }
  std::string bin_file = vm["bin"].as<std::string>();

  std::vector<std::string> inputs;
  if (vm.count("input-dir")) {
    inputs = vm["input-dir"].as<std::vector<std::string>>();
  }

  if (inputs.empty()) {
    std::cerr << "error: no inputs were specified.\n";
    std::cerr << desc;
    exit(1);
  }

  for (const std::string &x: inputs) {
    fs::path assets_dir(x);
    collect_directory_resources(assets_dir);
  }

  append_resources(bin_file);

  return 0;
}
