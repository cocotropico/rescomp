#include "c++/resources"
#include <list>
#include <string>
#include <iostream>

int main()
{
  resource_handle h;

  std::cout << "I contain " << h.file_count() << " resources.\n\n";

  std::cout << "This is my source code:\n";

  for (const std::string &file: h.file_list()) {
    std::cout << "\n--- " << file << "\n";

    resource_istream rs = h.get_file(file.c_str());
    std::cout << rs.rdbuf();
    std::cout << "\n";
  }

  return 0;
}
