
CXX := g++
CXXFLAGS := -g -O2 -std=gnu++11

all: rescomp
clean:
	rm -f rescomp
.PHONY: all clean

rescomp: rescomp.cc
	$(CXX) $(CXXFLAGS) -o $@ $^ -lboost_program_options -lboost_filesystem -lboost_system
